import logging as log
from models import *
import datetime
from datetime import timedelta
from nodes_handler import *
from binance_handler import *
from tools import Tools





class MoneroListener(Tools):
	def __init__(self):
		self.x = get_handler('XMR')
		self.h = Handler()
		self.logger = log.getLogger()
		
	



	def adjust_time(self,time,seconds):
		return time + timedelta(seconds=seconds)

	def get_address_indice(self,address):
		resp = self.x.send('get_address_index',address=address)
		return resp['result']['index']['minor']

	def get_mon_received(self,address):
		indice  = self.get_address_indice(address)
		resp_dict = self.x.send('getbalance',account_index=mon_index,address_indices=[indice])
		res = resp_dict['result']
		addresses =  res['per_subaddress']
		for addr in addresses:
			if address == addr['address']:
				balance = addr['balance']
				return balance

	def get_mon_txid(self,address):
		resp  = self.x.send('get_transfers',account_index=mon_index,in_=True)
		in_list =  resp['result']['in']
		for l in in_list:
			if l['address']  == address:
				return [l['txid'],l['amount']]
	

	def get_mon_confirmation(self,txid):
		resp = self.x.send('get_transfer_by_txid',txid=txid,account_index=mon_index)
		return resp['result']['transfer']['confirmations']
			
	def check_xmr_deposit(self,m):
		print('start check')
		print('your deposit address')
		print(m.deposit_address)
		amount = self.get_mon_received(m.deposit_address)
		if amount == 0:
			m.message = 'deposit not arrived yet'
			self.logger.info('deposit nor arrived yet')
			return False
		#amount = self.h.get_exchange_rate('XMR','BTC',0,( amount * (10**-12)))
		amount = amount * (10**-12)
		m.deposit_amount = amount
		m.save()
		txid_resp = self.get_mon_txid(m.deposit_address)
		txid = txid_resp[0]
		confirmations = int(self.get_mon_confirmation(txid))
		if amount < m.needed_deposit:
			m.message = 'The amount deposited is not equal to the mix amount defined which is {0} your deposited {1}'.format(str(m.mix_amount),str(amount))
			m.save()
		else:
			m.message = 'waiting for confirmatins current {0}'.format(str(confirmations))
			m.save()
		if confirmations >= 1:
			m.message = 'transaction confirmed and amount deposited'
			self.logger.info('transaction confirmed and amount deposited')
			m.deposited = True
			m.save()
			return True
		else:
			m.message = 'transaction not confirmed yet'
			self.logger.info('transaction not confirmed yet')
			return False

	

	def handle_xmr_mix(self,m):
		try:
			self.clean(m)
			limit = m.mix_limit - m.mix_single_size
			if (m.exchanged_amount + m.mix_single_size ) > limit:
				now = datetime.datetime.now()
				m.locktime = self.adjust_time(now,24 *3600)
				m.restart_time = self.adjust_time(now,m.mix_wait)
				m.exchanged_amount = 0
				m.save()
				self.logger.info('reached limit restarting after lock')
			elif m.total_paid == m.mix_amount:
				m.completed = True
				m.status = 'done'
				m.save()
				self.logger.info('mix completed')
			else:
				if m.mix_amount - m.received < m.mix_single_size:
					size = round(m.mix_amount - m.received,15)
				else:
					size = m.mix_single_size
				in_coin , out_coin = self.set_coin_mod(m.mix_mod)
				if not (m.started_exchange or m.exchanged or m.withdrawn) :
					res = self.h.handle_deposit(self.x,size,in_coin,m)
					self.logger.info(res)
					if res:
						self.logger.info('deposited successfuly to binance')
						m.last_single_size = size
						m.save()
						res2 = self.h.handle_exchange(m,in_coin,out_coin)
					else:
						self.logger.info('not deposited successfuly to binance')
						return False
				else:
					res2 = self.h.handle_exchange(m,in_coin,out_coin)
				if res2:
					self.logger.info('now on payouts')
					res3 = self.h.handle_payout(m,in_coin,out_coin)

					if res3:
						self.logger.info('mix {0} payout completed successfuly'.format(m.mix_id))
						m.message = 'done payout'
						m.started_exchange = False
						m.exchanged = False
						m.withdrawn = False
						m.save()
					else:
						self.logger.warning(' mix {0} payout failed'.format(m.mix_id))
						m.message = 'failed payout'
						m.save()

				else:
					self.logger.info('failed exchanging')



		
		except Exception as e:
			self.logger.exception('exception here at handle_btc_mix function level')
			return False




	

	