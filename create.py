from peewee import *
import os
from models import *
from playhouse.migrate import *
db = SqliteDatabase(os.getcwd()+'/mixs.db')

def create_db():

	
	db.create_tables([Mix,Payouts])
	print('created')

def add_column():
	migrator = SqliteMigrator(db)
	migrate(
		migrator.add_column('Mix','needed_deposit',FloatField(default=0)),
		
		
		
		

		
	)
	"""
	migrator.add_column('Mix','daily_rate',IntegerField(default = 0)),
		migrator.add_column('Mix','days',IntegerField(default = 0)),
		status = CharField(max_length=255,default='active')
		migrator.alter_column_type('Mix','completed',BooleanField(default=False))
		migrator.add_column('Mix','status',CharField(max_length=255,default='active'))
		migrator.add_column('Payouts','paid',BooleanField(default=False)),
		migrator.add_column('Payouts','amount_sent',FloatField(default=0)),
		migrator.alter_column_type('Payouts','mix',ForeignKeyField(Mix, backref='payouts'))
		"""

def delete_fields():
	pays = Payouts.select()
	for pay in pays:
		pay.delete().execute()

	mixs = Mix.select()
	for mix in mixs:
		mix.delete().execute()

def deactivate():
	mixs = Mix.select().where(Mix.status == 'active')
	for mix in mixs:
		mix.status = 'not active'
		mix.save()
	
	

if __name__ == '__main__':
	choice = input('you want to deactivate activated mixes ? (y/n) ==> ')
	#create_db()
	if 'y' in choice:

		deactivate()
	elif 'a' in choice:
		add_column()
	else:
		create_db()
	#add_column()
	
	#add_column()
	#delete_fields()