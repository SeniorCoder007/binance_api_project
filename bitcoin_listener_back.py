import logging as log
from models import *
import datetime
from datetime import timedelta
from nodes_handler import *
from binance_handler import *
from tools import Tools








class BitcoinListener(Tools):
	def __init__(self):
		self.b = get_handler('BTC')
		self.h = Handler()
		self.logger = log.getLogger()
		


	


	# btc function to get the txid of a transaction
	def get_btc_transaction(self,address,trs):
		txids = []
		for tr in trs:
			if tr['address'] == address:
				print('getting transaction')
				print(tr['txid'])
				txids.append(tr['txid'])
				

			else:
				pass

		return txids


	# get btc confirmations 
	def get_confirmations_worker(self,txids,m):
		confirmation = 999999
		
		for tx in txids:
			tr = self.b.send('gettransaction',tx)
			if int(tr['confirmations']) <= confirmation:
				confirmation = int(tr['confirmations'])
		return confirmation

	#btc confirmations
	def get_btc_confirmations(self,m):
		if m.txid != '':
			txids = m.txid.split(',')
		else:
			txids = self.get_btc_transaction(m.deposit_address,self.b.send('listtransactions','*',999999999))

		confirmations = self.get_confirmations_worker(txids,m)
		self.logger.info('{0}'.format(str(confirmations)))
		m.txid = ','.join(txids)
		m.confirmations = confirmations
		m.save()
		return confirmations
	
	

	def adjust_time(self,time,seconds):
		return time + timedelta(seconds=seconds)


	def check_btc_deposit(self,m):
		print('start check')
		print('your deposit address')
		print(m.deposit_address)
		amount = self.b.send('getreceivedbyaddress',m.deposit_address,0)
		if amount == 0:
			m.message = 'deposit not arrived yet'
			self.logger.info('deposit nor arrived yet')
			return False
		m.deposit_amount = amount
		m.save()
		confirmations = self.get_btc_confirmations(m)
		if amount < m.mix_amount:
			m.message = 'The amount deposited is not equal to the mix amount defined which is {0} your deposited {1}'.format(str(m.mix_amount),str(amount))
			m.save()
		else:
			m.message = 'waiting for confirmatins current {0}'.format(str(confirmations))
			m.save()
		if confirmations >= 1:
			m.message = 'transaction confirmed and amount deposited'
			self.logger.info('transaction confirmed and amount deposited')
			m.deposited = True
			m.save()
			return True
		else:
			m.message = 'transaction not confirmed yet'
			self.logger.info('transaction not confirmed yet')
			return False
	
	def handle_btc_payout(self,m,in_coin,out_coin):
		try:
			self.logger.info('now on payouts')
			res3 = self.h.handle_payout(m,in_coin,out_coin)
			if res3:
				self.logger.info('mix {0} payout completed successfuly'.format(m.mix_id))
				m.message = 'done payout'
				
				m.save()
			else:
				self.logger(' mix {0} payout failed'.format(m.mix_id))
				m.message = 'failed payout'
				m.save()
		except Exception as e:
			print(str(e))
			self.logger.exception('in the payout section')

	# handle mix after deposit
	def handle_btc_mix(self,m):
		try:
			limit = m.mix_limit - m.mix_single_size
			if (m.exchanged_amount + m.mix_single_size ) > limit:
				now = datetime.datetime.now()
				m.locktime = self.adjust_time(now,24 *3600)
				m.restart_time = self.adjust_time(now,m.mix_wait)
				m.exchanged_amount = 0
				m.save()
				self.logger.info('reached limit restarting after lock')
			elif ( m.binance_exchanged == m.mix_amount and m.withdrawn == False ):
				in_c , out_c = self.set_coin_mod(m.mix_mod)
				res = self.h.withdraw(m.mix_amount,out_c,m)
				if res:
					self.logger.info('successful withdraw')
				else:
					self.logger.error('failed withdraw')
			elif m.received == m.mix_amount:
				in_c , out_c = self.set_coin_mod(m.mix_mod)
				res = self.h.handle_payout(m,in_c,out_c)
				if res == 'continue':
					self.logger.info('payout passed for mix : {0}'.format(m.mix_id))

				elif res:
					m.completed = True
					m.status = 'done'
					m.save()
					self.logger.info('mix completed')
				else:
					self.logger.info('error at payout')
			else:
				if m.mix_amount - m.received < m.mix_single_size:
					size = m.mix_amount - m.received
				else:
					size = m.mix_single_size
				in_coin , out_coin = self.set_coin_mod(m.mix_mod)
				if not (m.started_exchange or m.exchanged or m.withdrawn) :
					res = self.h.handle_deposit(self.b,size,in_coin,m)
					self.logger.info(res)
					if res:
						self.logger.info('deposited successfuly to binance')
						m.last_single_size = size
						m.save()
						res2 = self.h.handle_exchange(m,in_coin,out_coin)
					else:
						self.logger.info('not deposited successfuly to binance')
						return False
				else:
					res2 = self.h.handle_exchange(m,in_coin,out_coin)
				if res2:
					self.logger.info('successful exchange')
				else:
					self.logger.info('failed exchanging')




				
				
			
		except Exception as e:
			self.logger.exception('exception here at handle_btc_mix function level')
			return False