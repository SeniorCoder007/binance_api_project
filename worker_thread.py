import threading
from models import *
from nodes_handler import *
from binance_handler import *
import logging
from bitcoin_listener import *
import datetime as dt
from datetime import datetime , timedelta
from tools import Tools
import time
from monero_listener import *






class ThreadWorker(threading.Thread,BitcoinListener,MoneroListener):
	def __init__(self):
		threading.Thread.__init__(self)
		self.b = get_handler('BTC')
		self.x = get_handler('XMR')
		self.h = Handler()
		logging.basicConfig(filename='app.log',level = logging.DEBUG, filemode='a', format='%(name)s - %(levelname)s - %(message)s')
		self.logger = logging.getLogger()
	

	


	def set_handler(self,coin):
		if coin == 'BTC':
			return self.b
		else:
			return self.x
	def get_balance(self,coin):
		return self.set_handler(coin).send('getbalance')


	def get_diff_secs(self,time1 , time2):
		diff = time2 - time1
		if diff.days < 0:
			return -1
		else:
			return (diff.days * 24 * 3600) + (diff.seconds)

	

	
	def check_deposit_handler(self,m):
		in_c , out_c  = self.set_coin_mod(m.mix_mod)
		if in_c == 'BTC':
			if m.deposited:
				now = dt.datetime.now()
				diff_res = self.get_diff_secs(now,m.restart_time)
				diff_res_2 = self.get_diff_secs(now,m.locktime)
				if diff_res <= 0:
					if diff_res_2 <= 0:
						res = self.handle_btc_mix(m)
					else:
						self.logger.info('still on lock time')
						return False
				else:
					self.logger.info('still on restart time')
					return False

			else:
				res = self.check_btc_deposit(m)
				if res:
					return m.deposited
				else:
					return m.deposited
		elif in_c == 'XMR':

			if m.deposited:
				now = dt.datetime.now()
				diff_res = self.get_diff_secs(now,m.restart_time)
				diff_res_2 = self.get_diff_secs(now,m.locktime)
				if diff_res <= 0:
					if diff_res_2 <= 0:
						res = self.handle_xmr_mix(m)
						if res:
							return True
						else:
							return False
					else:
						print('lock time')
						self.logger.info('still on lock time')
						return False
				else:
					print('rest time')
					self.logger.info('still on restart time')
					return False

			else:
				res = self.check_xmr_deposit(m)
				if res:
					res = self.handle_xmr_mix(m)
					if res:
						return True
					else:
						return False
				else:
					return m.deposited
		else:
			self.logger.error('coin not defined')


	def run(self):
		try:
			while True:
				for mix in Mix.select().where((Mix.status == 'active') & (Mix.completed == False)):
					self.logger.info('Mix : {0}'.format(mix.mix_id))
					print('Mix : {0}'.format(mix.mix_id))
					res = self.check_deposit_handler(mix)
					print('on mix level')
					self.logger.info('on mix level')
					self.logger.info(res)
					#input('Success')
					
				else:
					self.logger.info('empty list of mixes no mix is present')
				time.sleep(10)
				

		except Exception as e:
			self.logger.exception('exception at run level function')


if __name__ == '__main__':
	print('starting here')
	th = ThreadWorker()
	th.run()
			

		
		



	
