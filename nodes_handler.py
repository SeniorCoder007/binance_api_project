import requests as req
import json
from api_sett import *
from requests.auth import HTTPDigestAuth
import time
import logging


#needed variable

headers = {'content-type': 'application/json'}
payload = {"jsonrpc": "1.0", "id":"curltest", "method": "", "params": [] }
payload_2 = {"jsonrpc":"2.0","id":"0","method":"","params":{}}



class Bitcoin:
	def __init__(self):
		self.user = bitcoin_user
		self.password = bitcoin_password
		self.url = 'http://{0}:{1}@{2}:{3}/'.format(self.user,self.password,ip,b_port)
		self.logger = logging.getLogger()
		#print(self.url)

	
	def send(self,command,*args):
		if len(args) != 0:
			params = [x for x in args]
		else:
			params = []
		
		custom_payload = payload.copy()
		custom_payload['method'] = command
		custom_payload['params'] = params

		# sending and recieving response from the node
		while True:
			try:
				resp = req.post(self.url,json = custom_payload,headers = headers)
				break
			except Exception as e:
				print('request failed')
				print(str(e))
				time.sleep(5)
		data = json.loads(resp.content.decode())
		self.logger.info('node resp : {0} '.format(resp.content.decode()))
		if data['error']:
			self.logger.error(data)
			return False
		else:
			return data['result']

class Monero:
	def __init__(self):
		self.user  = monero_user
		self.password = monero_password
		#self.url = 'http://127.0.0.1:18083/json_rpc'.format(self.user,self.password)
		self.url = 'http://{0}:{1}/json_rpc'.format(ip2,m_port)
	def send(self,command,**kwargs):
		params = dict(kwargs)
		if 'in_' in params.keys():
			del params['in_']
			params['in'] = True

		#print(params)
		custom_payload = payload_2.copy()
		custom_payload['method'] = command
		custom_payload['params'] = params
		#print(custom_payload)
		while True:
			try:
				resp = req.post(self.url ,auth=HTTPDigestAuth(self.user,self.password), json=custom_payload,headers=headers)
				break
			except:
				print('request failed')
				time.sleep(5)
		#print(resp.content.decode())
		data = json.loads(resp.content.decode())
		#print(data)
		if 'error' in data.keys():
			print(data)
			print(data['error'])
			return False

		return data






def get_handler(coin):
	if coin == 'BTC':
		return Bitcoin()

	elif coin == 'XMR':
		return Monero()
	else:
		print('not recognized')
	






	