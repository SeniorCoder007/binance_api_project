






class Tools:
	def __init__(self):
		logging.basicConfig(filename='app.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')
		pass


	def set_coin_mod(self,mod):
		mod = int(mod)
		if mod == 0:
			in_c = 'BTC'
			out_c = 'XMR'
		elif mod == 1:
			in_c = 'XMR'
			out_c  = 'BTC'
		return in_c , out_c

	def format_monero(self,amount):
		amount = str(int(round(amount * (10**12),12)))
		diff = 12 - len(amount)
		amount = '0'*diff + amount
		return amount

	def clean(self,m):
		m.binance_exchanged = round(m.binance_exchanged , 15)
		m.deposit_amount = round(m.deposit_amount,15)
		m.exchanged_amount = round(m.exchanged_amount,15)
		m.last_single_size = round(m.last_single_size,15)
		m.received = round(m.received,15)
		m.total_paid = round(m.total_paid,15)
		m.save()
		

