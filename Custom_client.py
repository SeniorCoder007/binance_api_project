from binance.client import Client
from binance.exceptions import BinanceAPIException, BinanceRequestException, BinanceWithdrawException



class customClient(Client):


	# modifying the withdraw method


	def withdraw(self, **params):
		"""Submit a withdraw request.
		https://www.binance.com/restapipub.html
		Assumptions:
		- You must have Withdraw permissions enabled on your API key
		- You must have withdrawn to the address specified through the website and approved the transaction via email
		:param asset: required
		:type asset: str
		:type address: required
		:type address: str
		:type addressTag: optional - Secondary address identifier for coins like XRP,XMR etc.
		:type address: str
		:param amount: required
		:type amount: decimal
		:param name: optional - Description of the address, default asset value passed will be used
		:type name: str
		:param recvWindow: the number of milliseconds the request is valid for
		:type recvWindow: int
		:returns: API response
		.. code-block:: python
			{
				"msg": "success",
				"success": true,
				"id":"7213fea8e94b4a5593d507237e5a555b"
			}
		:raises: BinanceRequestException, BinanceAPIException, BinanceWithdrawException
		"""
		# force a name for the withdrawal if one not set
		#if 'asset' in params and 'name' not in params:
		#	params['name'] = params['asset']
		res = self._request_withdraw_api('post', 'withdraw.html', True, data=params)
		if not res['success']:
			raise BinanceWithdrawException(res['msg'])
		return res