#from binance.client import Client
from Custom_client import *
from api_sett import *
import math
import logging
from nodes_handler import *
import datetime as dt
from datetime import datetime , timedelta
from models import *
from tools import Tools

class Handler(Tools):
	def __init__(self):
		self.client = customClient(b_a_k,b_a_p)
		self.logger = logging.getLogger()


	def get_exchange_rate(self,in_c,out_c,fee,received):
		amount  = received
		if in_c == 'BCH':
			in_c  = 'BCH'#BCHABC''
		if out_c == 'BCH':
			out_c =   'BCH'#BCHABC''
		first_c = self.client.get_ticker(symbol='{0}USDT'.format(str(in_c)))
		second_c = self.client.get_ticker(symbol='{0}USDT'.format(str(out_c)))
		rate = float(first_c['lastPrice']) / float(second_c['lastPrice'])
		out_value = rate * (amount - (amount * (fee/100)))
		#out_value = rate * (amount)
		return out_value

	def get_exchange_rate_rec(self,in_c,out_c,marge,received): # exchange with a marge of 0.25%
		amount  = received
		if in_c == 'BCH':
			in_c  = 'BCH'#BCHABC''
		if out_c == 'BCH':
			out_c =   'BCH'#BCHABC''
		first_c = self.client.get_ticker(symbol='{0}USDT'.format(str(in_c)))
		second_c = self.client.get_ticker(symbol='{0}USDT'.format(str(out_c)))
		rate = float(first_c['lastPrice']) / float(second_c['lastPrice'])
		out_value = rate * (amount + (amount * (marge/100)))
		#out_value = rate * (amount)
		return out_value

	def get_precision(self,coin):
		if coin == 'BCH':
			coin = 'BCH'#BCHABC''
		print(coin)
		for x in self.client.get_symbol_info(symbol='{0}BTC'.format(coin))['filters']:
			if x['filterType'] == 'LOT_SIZE':
				return int(round(-math.log(float(x['stepSize']), 10), 0))


	def get_single(self,coin,amount):
		print(coin)
		if coin == 'BCH':
			coin = 'BCH'#BCHABC''
		tick = self.client.get_ticker(symbol='{0}USDT'.format(str(coin)))
		price = float(float(tick['lastPrice']) * amount)

		return price
	def get_usd_equiv(self,coin,amount): # get us how many coin we can get with a USD amount
		print(coin)
		if coin == 'BCH':
			coin = 'BCH'#BCHABC''
		tick = self.client.get_ticker(symbol='{0}USDT'.format(str(coin)))
		price = amount / float(tick['lastPrice'])
		return price
	def get_rate(self,coin,amount): # same as get_usd_equiv with rounded results (recommended)
		if coin == 'BCH':
			coin = 'BCH'#BCHABC''

		tick = self.client.get_ticker(symbol='{0}USDT'.format(str(coin)))
		price = float(tick['lastPrice'])
		price = round(amount / price,8)
		return price

	def get_inverse(self,coin,amount):
		if coin == 'BCH':
			coin = 'BCH'#BCHABC''

		tick = self.client.get_ticker(symbol='{0}USDT'.format(str(coin)))
		price = float(tick['lastPrice'])
		price = round(amount * price,8)
		return price

	def get_balance(self,coin):
		resp = self.client.get_asset_balance(asset=coin)
		return float(resp['free'])

	def mon_get_tr_fee(self,single,steps):
		m = get_handler('XMR')
		amnt = self.format_monero(single)
		resp = m.send('transfer',destinations=[{'amount':amnt,'address':monero_dummy}],account_index=mon_index,do_not_relay=True)
		if resp:
			return steps * (resp['result']['fee'] * (1 + 50/100))
		else:
			return 0.017	* steps


	def get_bitcoin_tr_fee(self,single,steps):
		#l = b.send('listunspent',1,999999)
		b = get_handler('BTC')
		#data_list = []
		data_list2 = [{bitcoin_dummy:single}]
		#counter = 1
		#for x in l:
			#data_list.append({'txid':x['txid'],'vout':x['vout']})

		raw_hex = b.send('createrawtransaction',[],data_list2)
		resp = b.send('fundrawtransaction',raw_hex)
		if resp:
			return resp['fee'] * steps
		else:
			return b.send('estimatesmartfee',2)['feerate'] * (350 /1000) * steps


	def generate_cusaddress(self,index):
		x = get_handler('XMR')
		resp = x.send('create_address',account_index=index)
		return resp['result']['address']


	def send_mon(self,amount,address,index):
		x = get_handler('XMR')
		resp = x.send('transfer',destinations=[{'amount':amount,'address':address}],account_index = index,unlock_time=0)
		return resp
		





	def estimate_fee(self,total,single,steps,coin):
		if coin == 1 :
			fees = round(steps * (single*(0.2/100)),3) +  (steps * 0.001)
			fees += steps * self.get_exchange_rate('BTC','XMR',0,bitcoin_w+0.00005) 
			fees += round(float(self.mon_get_tr_fee(single,steps))*10**-12,12)
			new_tot =  round(fees,12)
			
		elif coin == 0:
			fees = round(steps * (single*(0.2/100)),3) + (steps * 0.0001)
			fees += steps * self.get_exchange_rate('XMR','BTC',0,monero_w+0.00005)
			fees += round(float(self.get_bitcoin_tr_fee(single,steps)),8)
			new_tot =  round(fees,8)
			

		return new_tot





	


	# handle binance deposit
	def handle_deposit(self,n,amount,coin,m):
		try:
			dep_address = self.client.get_deposit_address(asset=coin)['address']
			#dep_address = '1LGBPV8fRuKLMSLw2hUMFjazKWXawaEgNa'
			logging.info('sending {0} to {1}'.format(str(amount),dep_address))
			if coin == 'BTC':
				fees = (amount*(0.15/100)) + 0.00008
				fees +=  self.get_exchange_rate('XMR','BTC',0,monero_w)
				amount += fees
				amount = round(amount,8)
				resp = n.send('sendtoaddress',dep_address,amount)
			else:
				#amount = self.get_exchange_rate('BTC','XMR',0,amount)

				fees = (amount*(0.15/100)) + 0.0008
				fees +=  self.get_exchange_rate('BTC','XMR',0,bitcoin_w) 
				#fees += round(float(self.mon_get_tr_fee(amount,1))*10**-12,12)
				amount += fees
				amount = round(amount,12)
				amount = self.format_monero(amount)
				
				resp = n.send('transfer',destinations=[{'amount':amount,'address':dep_address}],account_index = mon_index,unlock_time=0)
			if resp:
				logging.info('deposited to binance')
				m.started_exchange = True
				m.save()
				return resp
			else:
				logging.error('not deposited to binance')
				return False
		except Exception as e:
			self.logger.exception('exceptionat handle deposit level')

	# symbol XMRBTC 
	# market Buy Btc ==> XMR
	# market Sell XMR ==> BTC

	def withdraw(self,amount,out_c,m):
		try:
			n  = get_handler(out_c)
			old_amnt = amount
			if out_c == 'BTC':
				address = n.send('getnewaddress',label)
				withdraw_amnt = self.get_exchange_rate('XMR','BTC',0,amount)
				withdraw_amnt += bitcoin_w - 0.0001
				withdraw_amnt = round(withdraw_amnt,8)
			elif out_c == 'XMR':
				address = n.send('create_address',account_index = mon_index)
				address = address['result']['address']
				withdraw_amnt = self.get_exchange_rate('BTC','XMR',0,amount)
				withdraw_amnt += monero_w - 0.0001
				#withdraw_amnt = self.format_monero(withdraw_amnt)
				withdraw_amnt = round(withdraw_amnt,8)
			
			#withdraw_amnt += withdraw_amnt * (0.1 / 100)
			self.logger.info('withdrawing to {0} : {1} : {2} '.format(out_c,address,str(withdraw_amnt)))
			print('withdrawing')
			resp = self.client.withdraw(asset=out_c,address=address,amount=withdraw_amnt)
			if resp['success']:
				print('withdrawn')
				m.withdrawn = True
				m.received += old_amnt
				m.save()
				return True
			else:
				return False
		except Exception as e:
			self.logger.exception('withdraw exception')

	def handle_exchange(self,m,in_c,out_c):
		try:
			if m.started_exchange:
				if m.exchanged:
					pass
				else:
					balance = self.get_balance(in_c)
					if balance <= (m.last_single_size * ( 1+  0.2 / 100)):
						self.logger.error('not enough balance in binance checking later')
						return False
					else:
						try:
							if in_c == 'BTC':
								qt = self.get_exchange_rate('BTC','XMR',0,m.last_single_size)
								qt += 0.001 
								qt += (monero_w - 0.00005)
								qt = round(qt,3)
								self.client.order_market_buy(symbol= 'XMRBTC',quantity = qt)
							else:
								qt = round(m.last_single_size* ( 1+  0.15 / 100) + self.get_exchange_rate('BTC','XMR',0,bitcoin_w - 0.00005), 3 ) 
								self.client.order_market_sell(symbol= 'XMRBTC',quantity = qt )
							m.binance_exchanged += m.last_single_size
							print(m.binance_exchanged)
							# code for the payout with each exchange versionn
							m.exchanged = True
							m.save()
							######

							# code version for the payout after the whole exchange version 

					
							#if m.binance_exchanged == m.mix_amount:
							#	m.exchanged = True
							#else:
							#	now = dt.datetime.now()
							#	res_time = self.adjust_time(now,m.mix_wait)
							#	m.restart_time = res_time
							#	m.started_exchange = False
							#m.save()
						except Exception as e:
							self.logger.exception('in exchange level')
							return False


				if m.withdrawn:
					pass
				else:
					w_res = self.withdraw(m.last_single_size,out_c,m)
					if w_res:
						self.logger.info('withdraw success')
					else:
						self.logger.warning('withdraw error')

			



				return True

			else:
				self.logger.info('need deposit first')
				return False
			
		except Exception as e:
			self.logger.exception('in handle exchange')
			return False
	

	def adjust_time(self,time,seconds):
		return time + timedelta(seconds=seconds)

	def clean_addresses(self,m):
		pays = m.payouts
		for pay in pays:
			pay.address = pay.address.strip().replace('"','').replace("'","").replace('\n','')
			pay.save()
		return True

	def handle_payout(self,m,in_c,out_c):
		try:
			n = get_handler(out_c)
			if self.clean_addresses(m):
				print('cleaned')
			while True:
				pays = m.payouts.where(Payouts.paid == False)
				if len(pays) == 0:
					pays = m.payouts
					for pay in pays:
						pay.paid = False
						pay.save()
						continue
				else:
					pay = pays[0]
					break

			amount = self.get_exchange_rate(in_c,out_c,0,m.last_single_size)
			total = self.get_exchange_rate(in_c,out_c,0,m.mix_amount)
			total_paid  = self.get_exchange_rate(in_c,out_c,0,m.total_paid)
			if (total_paid + amount) > total:
				amount = total - total_paid

			if out_c == 'XMR':
				o_amount = amount
				fees = round(float(self.mon_get_tr_fee(amount,1))*10**-12,12)
				amount -= fees
				amount = self.format_monero(amount)
				self.logger.info('[{0}] Sending {1} to {2}'.format(out_c,str(amount),pay.address))
				res = n.send('transfer',destinations = [{'amount':amount,'address':pay.address}],account_index = mon_index)
				if res:
					self.logger.info('transaction paid out')
					pay.amount_sent += o_amount
					pay.paid = True
					pay.save()
					m.total_paid += m.last_single_size
					#now = dt.datetime.now()
					#res_time = self.adjust_time(now,m.mix_wait)
					#m.restart_time = res_time
					m.save()
					return True
					if m.total_paid == total:
						return True
					else:
						return 'continue'
				else:
					self.logger.info('transaction failed')
					return False
			else:
				amount = round(amount,8)
				self.logger.info('[{0}] Sending {1} to {2}'.format(out_c,str(amount),pay.address))
				res = n.send('sendtoaddress',pay.address,amount,'','',True)
				if res:
					self.logger.info('transaction paid out')
					pay.amount_sent += amount
					pay.paid = True
					pay.save()
					m.total_paid += m.last_single_size
					#now = dt.datetime.now()
					#res_time = self.adjust_time(now,m.mix_wait)
					#m.restart_time = res_time
					m.save()
					return True
					if m.total_paid == total:
						return True
					else:
						return 'continue'
				else:
					self.logger.info('transaction failed')
					return False

		
		except Exception as e:
			self.logger.exception('on payout function')
			

	




"""

h.client.withdraw(asset='XMR',address='46qiKNCBNMK3BevyYfK3ZAgAqxkMGUMRhA5E5ursCavTUfYTLTiuZFv2NKhWKczbJ9fTdMoJTsBVxaxtxKXT6xHvE3rkpxR',amount=round(h.get_exchange_rate('BTC','XMR',0,0.0001),12))



"""