from peewee import *
import datetime
import os

db = SqliteDatabase(os.getcwd()+'/mixs.db', pragmas={'foreign_keys': 1})



class Mix(Model):
	mix_id = CharField(max_length  =255)
	status = CharField(max_length=255,default='active')
	mix_mod = IntegerField(default=0)
	mix_amount = FloatField(default= 0)
	needed_deposit = FloatField(default= 0)
	mix_single_size = FloatField(default = 0)
	mix_wait = FloatField(default = 0)
	mix_limit = FloatField(default = 0)
	created = DateTimeField(default = datetime.datetime.now)
	restart_time = DateTimeField(default = datetime.datetime.now)
	locktime = DateTimeField(default=datetime.datetime.now)
	exchanged_amount = FloatField(default = 0) # already exchanged that day max is the mix limit
	binance_exchanged = FloatField(default=0) # the exchanged amount to keep track
	received = FloatField(default=0) # the amount paid out
	deposited = BooleanField(default=False)
	completed = BooleanField(default=False)
	duration = IntegerField(default=0)
	daily_rate = IntegerField(default = 0)
	days = IntegerField(default = 0)
	confirmations = IntegerField(default=0)
	deposit_address = CharField(max_length=255,default='')
	deposit_amount = FloatField(default=0) # amount deposited
	message = CharField(max_length=2000,default='')
	txid = CharField(max_length=300,default='')
	# toggles in between exchange
	started_exchange = BooleanField(default=False)
	exchanged = BooleanField(default=False)
	withdrawn = BooleanField(default=False)
	last_single_size = FloatField(default = 0)
	total_paid = FloatField(default=0)

	class Meta:
		database  = db



class Payouts(Model):
	mix = ForeignKeyField(Mix, backref='payouts')
	address = CharField(max_length=255,default='')
	txid = CharField(max_length=255,default='')
	paid = BooleanField(default=False)
	amount_sent = FloatField(default=0)

	class Meta:
		database = db


