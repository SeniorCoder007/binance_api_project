from binance_handler import *
from nodes_handler import *
from models import *
import random
import string
from peewee import *
import datetime
import os
import logging
from worker_thread import ThreadWorker




#in_c is the coin we are sending to the mixer
#out_c is the coin we are getting out of the mixer



class worker:
	def __init__(self):
		self.h = Handler()
		self.b = get_handler('BTC')
		self.x = get_handler('XMR')
		self.db = SqliteDatabase(os.getcwd()+'/mixs.db')
		self.hours = 23
		logging.basicConfig(filename='app.log',level = logging.DEBUG, filemode='a', format='%(name)s - %(levelname)s - %(message)s')
		

	def generate_code(self,length=10):
		choice = [1,2,3]
		x = 1
		final_code = ''
		while x <= length:
			c = random.choice(choice)
			if c == 1:
				final_code += str(random.choice(string.ascii_lowercase))
			elif c == 2:
				final_code += str(random.choice(string.ascii_uppercase))
			else:
				final_code += str(random.randint(0,9))

			x += 1

		return final_code


	def set_coin_mod(self,mod):
		mod = int(mod)
		if mod == 0:
			in_c = 'BTC'
			out_c = 'XMR'
		elif mod == 1:
			in_c = 'XMR'
			out_c  = 'BTC'
		return in_c , out_c


	def get_days(self,m):
		try:
			print(m.mix_amount)
			print(m.completed)
			rate = m.daily_rate
			days = m.mix_amount // rate
			print(rate)
			print(m.mix_amount % rate)
			if m.mix_amount % rate == 0:
				pass
			else:
				days += 1

			return days
			
			
		except Exception as e:
			print(str(e))
	
	def delete_mix(self,mix):
		try:
			mix.delete().execute()
			return True
		except Exception as e:
			logging.error(str(e))
			return False

	
	def get_deposit_address(self,coin):
		if coin == 'BTC':
			return self.b.send('getnewaddress',label)
		elif coin == 'XMR':
			resp = self.x.send('create_address',account_index=mon_index)
			return resp['result']['address']
		else:
			logging.error('coin not identified')



	




	def main(self):
		c = input('you want to make a new mix (y/n) ? ==> ')
		if c == 'y':
			print('#'*5 + 'Welcome to Mix Script' + '#'*5)
			print('start by set up the exchange ')
			choice1 = int(input('exchange BTC ==> XMR (type 0) or XMR ==> BTC (type 1)\n==> '))
			choice2 = float(input('amount to be exchanged ==> '))
			choice5 = float(input('current limit (in BTC) ==> '))
			in_c , out_c = self.set_coin_mod(choice1)
			if in_c == 'XMR':
				choice5 = self.h.get_exchange_rate('BTC','XMR',0,choice5)
			passed = False
			while not passed:

				choice3 = float(input('size single ==> '))
				choice4 = float(input('wait time in sec ==> '))
				limit = choice5-choice3
				daily_tr_rate = limit // choice3
				if limit % choice3 == 0:
					pass
				else:
					daily_tr_rate += 1
				daily_rate = (self.hours*3600) // choice4
				
				
				if daily_tr_rate <= daily_rate:
					passed = True
					break
				else:
					daily_tr_rate = daily_rate
					passed = True
					break
			payouts = input('Input the payouts addresses comma separated (,) \n ==> ')
				
			steps_t = choice2 // choice3
			if choice2 % choice3 != 0:
				steps_t += 1
			print('saving the exchange settings ')
			# generate the mix ID
			idd = self.generate_code(length=24)
			# get in and out coins based on the mode

			fees = self.h.estimate_fee(choice2,choice3,steps_t,choice1)

			total = round(choice2 + fees , 8)

			print('### Your Transaction details :')
			print('mix amount : ' + str(choice2))
			print('fees : ' + str(fees))
			print('Deposit amount needed : ' + str(total))
			
			
			# get deposit address
			deposit_add = self.get_deposit_address(in_c)

			# saving the mix details
			
			m = Mix.create(mix_id=idd,mix_mod = choice1,mix_amount = choice2,needed_deposit = total,mix_single_size = choice3,mix_wait=choice4,mix_limit = limit,daily_rate = daily_tr_rate, deposit_address = deposit_add)
			m.save()

			# filtering and saving the payouts addresses
			payouts_container = payouts.split(',')
			for pay in payouts_container:
				p = Payouts.create(mix= m , address= pay)
				p.save()

			exchanged_ex = self.h.get_exchange_rate(in_c,out_c,0,choice2)
			if out_c == 'BTC':
				precision = 8
			else:
				precision = self.h.get_precision(out_c)
			print('amount to be expected {0} {1}'.format(str(round(exchanged_ex,precision)),out_c))
			days = self.get_days(m)
			print('the exchange will take {0} (day / days) '.format(str(days)))
			m.days = days
			m.save()
			print('your mix ID : {0}  ; Save It'.format(m.mix_id))
		else:
			print('start thread')
			t = ThreadWorker()
			
			t.start()
			logging.info('process starting')





if __name__ == '__main__':
	w = worker()
	w.main()
